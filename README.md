# 星萌印票姬

#### 介绍
为了配套全新的AES加密版检票姬，特此制作。
可以生成对应新版需求的密钥与票卡数据库

#### 软件架构
单机运行，其余不懂。


#### 安装教程

1.  找个文件夹解压。


#### 使用说明

1.  生成AES密钥。
2.  输入票号数量以及前缀。
3.  使用下方检验栏检查密钥与票号是否对应。
4.  将cutestar.key与星萌DB.txt复制到AES版的星萌检票姬目录使用。
5.  记得把星萌DB的内容发给票卡厂商。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
